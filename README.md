# Technical Assessment


## Software Required

You will need to install Vagrant and either Docker or Virtualbox to run the  
technical assessment. If you have trouble with Docker then try Virtualbox  
instead.

[Git](https://git-scm.com/)  
[Vagrant](https://www.vagrantup.com)  
[Docker](https://www.docker.com) or [Virtualbox](https://www.virtualbox.org/)

**Please ensure you fork this Git repository and clone your fork.**  

You will need to allow access to your fork of the repository for the  
assessment review. If you want to keep your fork private then please grant  
access to the group `cloudsolutions`.

The assessment has been tested with the following OS and software versions:

Fedora 26 + Vagrant 2.0.1 + Docker 1.13.1  
Fedora 25 + Vagrant 1.9.1 + Docker 1.12.6  
Fedora 25 + Vagrant 1.9.1 + Virtualbox 5.1.12  
macOS Sierra 10.12.1 + Vagrant 1.9.1 + Virtualbox 5.1.14  
macOS Sierra 10.12.3 + Vagrant 1.9.1 + Virtualbox 5.0.10  

If you successfully complete the assessment with other combinations then  
please let us know the details.

## Getting Started

To start the technical assessment run either one of the following commands:  
`vagrant up --provider docker`  
`vagrant up --provider virtualbox`

You will see Vagrant start up the container or virtual machine, this might  
take some time on your machine as you will likely need to download images. At  
some point you should see the provisioning process start and Puppet running.  
This could take a little time to complete.

Once provisioning is complete, connect to the [app](http://localhost:8080).
