class profiles::global {

  class { 'timezone':
    region   => 'Europe',
    locality => 'London',
  }

  class { 'firewall':
    ensure => 'stopped',
  }

  class { 'motd':
    dynamic_motd => 'yes',
  }

}
