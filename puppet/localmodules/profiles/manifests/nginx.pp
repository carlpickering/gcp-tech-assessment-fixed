# Add NGINX web server as proxy in front of the app
class profiles::nginx {

  class{ "nginx":
    manage_repo    => false,
    package_source => 'nginx-mainline',
    proxy_cache_path  => '/var/lib/nginx/cache levels=1:2 keys_zone=appcache:8m max_size=50m',
  }

  nginx::resource::upstream { 'the_app':
    members => [
      '127.0.0.1:3000',
      ],
    }

    file { '/var/lib/nginx/cache':
      ensure => 'directory',
      owner => 'nginx',
      group => 'nginx',
      mode => '0700',
    }

  nginx::resource::server{'localhost.localdomain':
      www_root          => '/opt/html/',
      proxy_cache_valid => '404 1m',
    }

  nginx::resource::location{'/app':
    proxy => 'http://the_app/',
    server => 'localhost.localdomain',
    proxy_cache => 'appcache',
}
}
